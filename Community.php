<?php

namespace Houserich\Models;

class Community extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $communityId;

    /**
     * @comment('社區名稱')
     * @var string
     */
    public $name;

    /**
     * @comment('建設公司')
     * @var string
     */
    public $company;

    /**
     * @comment('公共設施')
     * @var string
     */
    public $publicFacility;

    /**
     * @comment('建材')
     * @var string
     */
    public $meterials;

    /**
     * @comment('地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * @comment('完成日期')
     * @var string
     */
    public $completion;

    /**
     * @comment('屋齡(年)')
     * @var integer
     */
    public $houseAgeYear;

    /**
     * @comment('屋齡(月)')
     * @var integer
     */
    public $houseAgeMonth;

    /**
     * @comment('總層數（地上）')
     * @var integer
     */
    public $floorAbove;

    /**
     * @comment('總層數（地下）')
     * @var integer
     */
    public $floorBelow;

    /**
     * @comment('路寬(m)')
     * @var integer
     */
    public $roadWidth;

    /**
     * @comment('電梯數量')
     * @var integer
     */
    public $numElevators;

    /**
     * @comment('1樓現況')
     * @var string
     */
    public $onefloor;

    /**
     * @comment('基地面積')
     * @var double
     */
    public $baseArea;

    /**
     * @comment('棟數')
     * @var integer
     */
    public $numBuildings;

    /**
     * @comment('戶數')
     * @var integer
     */
    public $numHousehold;

    /**
     * @comment('簡介')
     * @var string
     */
    public $description;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('communityId', 'Houserich\Models\CommunityMrt', 'CommunityId', array('alias' => 'CommunityMrt'));
        $this->hasMany('communityId', 'Houserich\Models\RichitemCommunitymrt', 'CommunityId', array('alias' => 'RichitemCommunitymrt'));
    }


    /**
     * 處理完整地址組合配置
     * */
    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane;
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley;
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNoEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidation()
    {
        $this->setAddress();

        // html
        $description = $this->description;
        $this->description = htmlspecialchars($description, ENT_QUOTES, 'UTF-8');

        // 判斷寫入fieldoptions
        // 公共設施
        foreach($this->publicFacility as $i => $item) {
            if( $item == '其他' ){
                $fieldname='公共設施';
                $value = $_POST['publicFacilityothers'];
                $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
                if( !$f ){
                    $f = new \Houserich\Models\Fieldoptions();
                    $f->fieldname = $fieldname;
                    $f->label = $value;
                    $f->value = $value;
                    $f->save();
                }
                $this->publicFacility[$i] = $value;
            }
        }
        $this->publicFacility = implode(',', $this->publicFacility);

        // 建材
        if( $this->meterials == '其他' ){
            $fieldname='建材';
            $value = $_POST['meterialsothers'];
            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
                $f->fieldname = $fieldname;
                $f->label = $value;
                $f->value = $value;
                $f->save();
            }
            $this->meterials = $value;
        }

        /**
         * use iterator loop for filter special columns
         * */
        foreach ($this as $key => $value) {
            if( in_array($key, ["addressLane", "addressAlley", "addressNo", "numElevators", "numBuildings", "numHousehold"]) ){
                $this->{$key} = $this->getDI()->get('filter')->sanitize($value, "zerotonull");
            }
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'community';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Community[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Community
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
