<?php

namespace Houserich\Models;

class CommunityMedia extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $mediaId;

    /**
     * @comment('關聯社區大樓編號')
     * @var integer
     */
    public $CommunityId;

    /**
     * @comment('媒體類型')
     * @var string
     */
    public $mime;

    /**
     * @comment('檔案儲存路徑')
     * @var string
     */
    public $filepath;

    /**
     * @comment('存取識別碼')
     * @var string
     */
    public $UUID;

    /**
     * @comment('原始檔名')
     * @var string
     */
    public $originname;

    /**
     * @comment('檔案大小(Bytes)')
     * @var integer
     */
    public $size;

    /**
     * @comment('縮圖路徑')
     * @var string
     */
    public $thumbnail;

    /**
     * @comment('建檔時間')
     * @var integer
     */
    public $setTime;

    /**
     * @comment('狀態碼')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('排序')
     * @var integer
     */
    public $weight;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('CommunityId', 'Houserich\Models\Community', 'communityId', array('alias' => 'Community'));
        $this->belongsTo('CommunityId', 'Houserich\Models\Community', 'communityId', array('foreignKey' => true,'alias' => 'Community'));
    }


    public function beforeValidation()
    {
        $this->setTime = time();
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CommunityMedia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CommunityMedia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'community_media';
    }

}
