<?php

namespace Houserich\Models;

class Crawldata extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $crawldataId;

    /**
     * @Comment("標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("存取來源")
     *
     * @var string
     */
    public $sourcelink;

    /**
     * @Comment("封面圖")
     *
     * @var string
     */
    public $cover;

    /**
     * @Comment("縣市")
     *
     * @var string
     */
    public $addressCity;

    /**
     * @Comment("行政區")
     *
     * @var string
     */
    public $addressDistrict;

    /**
     * @Comment("原始抓取地址")
     *
     * @var string
     */
    public $addressSegment;

    /**
     * @Comment("顯示地址")
     *
     * @var string
     */
    public $addressShow;

    /**
     * @Comment("總價")
     *
     * @var double
     */
    public $priceTotal;

    /**
     * @Comment("降價總價")
     *
     * @var double
     */
    public $priceDown;

    /**
     * @Comment("類型")
     *
     * @var string
     */
    public $type;

    /**
     * @Comment("用途")
     *
     * @var string
     */
    public $usefor;

    /**
     * @Comment("房數")
     *
     * @var integer
     */
    public $patternRoom;

    /**
     * @Comment("廳數")
     *
     * @var integer
     */
    public $patternHall;

    /**
     * @Comment("衛浴數")
     *
     * @var integer
     */
    public $patternBathroom;

    /**
     * @Comment("陽台數")
     *
     * @var integer
     */
    public $patternBalcony;

    /**
     * @Comment("所在樓層")
     *
     * @var integer
     */
    public $floorAt;

    /**
     * @Comment("總樓層")
     *
     * @var integer
     */
    public $floorTotal;

    /**
     * @Comment("屋齡")
     *
     * @var double
     */
    public $houseage;

    /**
     * @Comment("有無車位")
     *
     * @var integer
     */
    public $parking;

    /**
     * @Comment("總坪數")
     *
     * @var double
     */
    public $areaTotal;

    /**
     * @Comment("不含車位坪數")
     *
     * @var double
     */
    public $areaMain;

    /**
     * @Comment("車位坪數")
     *
     * @var double
     */
    public $areaParking;

    /**
     * @Comment("緯度")
     *
     * @var double
     */
    public $lat;

    /**
     * @Comment("經度")
     *
     * @var double
     */
    public $lng;

    /**
     * @Comment("地圖網址")
     *
     * @var string
     */
    public $sourceMap;

    /**
     * @Comment("社區")
     *
     * @var string
     */
    public $community;

    /**
     * @Comment("物件抓取時間")
     *
     * @var integer
     */
    public $initTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setConnectionService("dbCrawler");

        // $this->hasMany('crawldataId', 'CrawldataExtendinfo', 'CrawldataId', array('alias' => 'CrawldataExtendinfo'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'crawldata';
    }


    /**
     * @todo : 總價：若有降價時顯示降價圖示（六個月內有降價）
     * */
    public function afterFetch()
    {
        $tmp=array();
        if( !empty($this->patternRoom) ){
            $tmp[]=$this->patternRoom.'房';
        }

        if( !empty($this->patternHall) ){
            $tmp[]=$this->patternRoom.'廳';
        }

        if( !empty($this->patternBathroom) ){
            $tmp[]=$this->patternRoom.'衛';
        }

        if( !empty($this->patternBalcony) ){
            $tmp[]=$this->patternRoom.'陽台';
        }

        $this->patterns=implode('', $tmp);

        $strtool = new \Personalwork\Package\StringTools();
        $this->title=$strtool->utf8_cutstr(trim($this->title), 16);
        // $tmp_title=$strtool->utf8_str_split(trim($this->title), 8);
        // if( count($tmp_title) > 1 ){
        //     $this->title = $tmp_title[0].'...';
        // }else{
        //     $this->title = $tmp_title[0];
        // }

        // 總價：取整數
        $this->priceTotal = round($this->priceTotal);

        // 處理地址顯示
        if( empty($this->addressShow) ){
            preg_match("/(.+[路|段|道|街|村|巷]).*/u", $this->addressSegment, $m);
            $this->addressShow=$m[1];
        }

        // 處理坪數：若小數點為0則不顯示、符號使用全形
        $this->areaTotal = preg_replace('/[.|0]+$/', '', $this->areaTotal);
        if( $this->areaParking ){
            $this->areaMain = preg_replace('/[.|0]+$/', '', $this->areaMain);
            $this->areaParking = preg_replace('/[.|0]+$/', '', $this->areaParking);
        }


        /**
         * 判斷是否新上架
         * 根據initTime時間若在一週內則設定標籤「newinweek」
         * */
        if( !empty($this->initTime) && $this->initTime+(7*86400) >= time() ){
            $this->newinweek=true;
        }else{
            $this->newinweek=false;
        }

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Crawldata[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Crawldata
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
