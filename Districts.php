<?php

namespace Houserich\Models;

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Districts extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $districtId;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $city;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $district;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $zone;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $zonesort;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'districts';
    }

    /**
     * 直接使用rawsql取得根據區域配置的選項結構
     * array (size=5)
          '北部' =>
            array (size=6)
              0 => string '基隆市' (length=9)
              1 => string '台北市' (length=9)
              2 => string '新北市' (length=9)
              3 => string '桃園市' (length=9)
              4 => string '新竹市' (length=9)
              5 => string '新竹縣' (length=9)
          '中部' =>
            array (size=5)
              0 => string '苗栗縣' (length=9)
              1 => string '臺中市' (length=9)
              2 => string '彰化縣' (length=9)
              3 => string '南投縣' (length=9)
              4 => string '雲林縣' (length=9)
          '南部' =>
            array (size=5)
              0 => string '嘉義市' (length=9)
              1 => string '嘉義縣' (length=9)
              2 => string '臺南市' (length=9)
              3 => string '高雄市' (length=9)
              4 => string '屏東縣' (length=9)
          '東部' =>
            array (size=3)
              0 => string '宜蘭縣' (length=9)
              1 => string '花蓮縣' (length=9)
              2 => string '臺東縣' (length=9)
          '離島' =>
            array (size=3)
              0 => string '澎湖縣' (length=9)
              1 => string '金門縣' (length=9)
              2 => string '連江縣' (length=9)
     * */
    public function getZones()
    {
        $rawsql = "SELECT zone, GROUP_CONCAT( DISTINCT(`city`) ) AS cities FROM districts GROUP BY zone ORDER BY zonesort";
        $d = new Districts();
        // Execute the query
        $rows = new Resultset(
            null,
            $d,
            $d->getReadConnection()->query($rawsql)
        );
        $tmp=array();
        foreach($rows->toArray() as $i => $data) {
            $tmp[$data['zone']]=explode(',', $data['cities']);
        }

        return $tmp;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Districts[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Districts
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
