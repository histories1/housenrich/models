<?php

namespace Houserich\Models;

class Fieldoptions extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $id;

    /**
     * @comment('欄位名稱')
     * @var string
     */
    public $fieldname;

    /**
     * @comment('資料標籤')
     * @var string
     */
    public $label;

    /**
     * @comment('資料')
     * @var string
     */
    public $value;

    /**
     * @comment('關聯上層主鍵')
     * @var integer
     */
    public $parentId;

    /**
     * @comment('關聯上層欄位')
     * @var string
     */
    public $parentLabel;

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Fieldoptions[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Fieldoptions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'fieldoptions';
    }

}
