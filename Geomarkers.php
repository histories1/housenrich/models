<?php

namespace Houserich\Models;

class Geomarkers extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $geomarkersId;

    /**
     * @comment('地標名稱')
     * @var string
     */
    public $marklabel;

    /**
     * @comment('詳細地址')
     * @var string
     */
    public $markaddress;

    /**
     * @comment('經度')
     * @var double
     */
    public $longitude;

    /**
     * @comment('緯度')
     * @var double
     */
    public $latitude;

    /**
     * @comment('座標類型')
     * @var string
     */
    public $type;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'geomarkers';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Geomarkers[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Geomarkers
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
