<?php

namespace Houserich\Models;

class Invheader extends \Phalcon\Mvc\Model
{

    /**
     * @comment('')
     * @var integer
     */
    public $invid;

    /**
     * @comment('')
     * @var string
     */
    public $invdate;

    /**
     * @comment('')
     * @var integer
     */
    public $client_id;

    /**
     * @comment('')
     * @var double
     */
    public $amount;

    /**
     * @comment('')
     * @var double
     */
    public $tax;

    /**
     * @comment('')
     * @var double
     */
    public $total;

    /**
     * @comment('')
     * @var string
     */
    public $closed;

    /**
     * @comment('')
     * @var string
     */
    public $ship_via;

    /**
     * @comment('')
     * @var string
     */
    public $note;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'invheader';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Invheader[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Invheader
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
