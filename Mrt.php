<?php

namespace Houserich\Models;

class Mrt extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $mrtId;

    /**
     * @comment('站名')
     * @var string
     */
    public $name;

    /**
     * @comment('路線名稱')
     * @var string
     */
    public $lineLabel;

    /**
     * @comment('路線代碼')
     * @var string
     */
    public $lineId;

    /**
     * @comment('地區')
     * @var string
     */
    public $city;

    /**
     * @comment('區別')
     * @var string
     */
    public $district;

    /**
     * @comment('站名別名')
     * @var string
     */
    public $nameAlias;


    /**
     * @comment('通車狀態')
     * @var string
     */
    public $statecode;

    var $statecodeLabels = ['未通車', '已通車'];


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('mrtId', 'Houserich\Models\RichitemMrt', 'MrtId', array('alias' => 'RichitemMrt'));
        $this->hasMany('mrtId', 'Houserich\Models\RichitemMrt', 'MrtId', NULL);
    }


    public function getStatecodeLabel(){
        return $this->statecodeLabels[$this->statecode];
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mrt';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Mrt[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Mrt
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
