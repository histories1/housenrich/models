<?php

namespace Houserich\Models;

class People extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $peopleId;

    /**
     * @comment('帳號類型')
     * @var string
     */
    public $type;

    /**
     * @comment('手機門號')
     * @var string
     */
    public $cellphone;

    /**
     * @comment('認證手機')
     * @var integer
     */
    public $vertifyCellphone;

    /**
     * @comment('註冊時間')
     * @var integer
     */
    public $initTime;


    /**
     * @comment('第三方認證識別碼')
     * @var integer
     */
    public $externalID;

    const TYPE_LOCAL = 'local';

    const TYPE_FACEBOOK = 'facebook';

    const TYPE_YAHOO = 'yahoo';

    const TYPE_LINE = 'line';

    var $typeLabels = ['local'=>"本地", 'facebook'=>"Facebook", 'yahoo'=>"Yahoo", 'line'=>"Line"];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasOne('peopleId', 'Houserich\Models\PeopleInformation', 'PeopleId', array('alias' => 'PeopleInformation'));
        $this->hasOne('peopleId', 'Houserich\Models\PeopleInvoice', 'PeopleId', array('alias' => 'PeopleInvoice'));
        $this->hasMany('peopleId', 'Houserich\Models\PeopleRole', 'PeopleId', array('alias' => 'PeopleRole'));
        $this->hasMany('peopleId', 'Houserich\Models\PeopleStatus', 'PeopleId', array('alias' => 'PeopleStatus'));
    }


    public function getType($label) {
        return array_search($label, $this->typeLabels);
    }

    public function getTypeLabel($type=null){
        if( isset($type) ){
            return $this->typeLabels[$type];
        }else{
            return $this->typeLabels[$this->type];
        }
    }

    public function beforeValidationOncreate()
    {
        // 註冊時間
        $this->initTime = time();
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add( 'cellphone',
            new \Phalcon\Validation\Validator\Uniqueness([
                'model' => $this,
                'allowEmpty'=> true,
                'message' => '抱歉！您輸入的門號已經被使用，請重新輸入。',
            ])
        );

        return $this->validate($validator);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return People[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return People
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people';
    }

}
