<?php

namespace Houserich\Models;

class PeopleInformation extends \Phalcon\Mvc\Model
{

    /**
     * @comment('')
     * @var integer
     */
    public $PeopleId;

    /**
     * @comment('帳號')
     * @var string
     */
    public $account;

    /**
     * @comment('密碼')
     * @var string
     */
    public $password;

    /**
     * @comment('姓名')
     * @var string
     */
    public $fullName;

    /**
     * @comment('身分證字號')
     * @var string
     */
    public $ID;

    /**
     * @comment('性別')
     * @var string
     */
    public $gender;

    /**
     * @comment('年齡')
     * @var string
     */
    public $age;

    /**
     * @comment('婚姻')
     * @var string
     */
    public $marriage;

    /**
     * @comment('成員人數')
     * @var string
     */
    public $members;

    /**
     * @comment('職業')
     * @var string
     */
    public $occupation;

    /**
     * @comment('年收入(單位:萬)')
     * @var double
     */
    public $annualIncome;

    /**
     * @comment('通訊地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * @comment('電子信箱')
     * @var string
     */
    public $email;

    /**
     * @comment('LINE帳號')
     * @var string
     */
    public $lineID;

    /**
     * @comment('最後狀態')
     * @var integer
     */
    public $lastStatecode;

    /**
     * @comment('最後上線時間')
     * @var integer
     */
    public $lastLoginTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));

        $this->skipAttributesOnCreate(['lastStatecode', 'lastLoginTime']);
    }


    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane.'巷';
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley.'弄';
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNo.'號';
        }
        if( !empty($this->addressNoEx) ){
            $addr[] = '之'.$this->addressNoEx;
        }
        if( !empty($this->addressFloor) ){
            $addr[] = $this->addressFloor.'樓';
        }
        if( !empty($this->addressFloorEx) ){
            $addr[] = '之'.$this->addressFloorEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidationOnCreate()
    {
        // 密碼加密
        $this->password = $this->getDI()->get('security')->hash($this->password);
    }


    public function beforeValidationOnUpdate()
    {
        if( !empty($_POST['password']) ){
            $this->skipAttributesOnUpdate(['account', 'lastStatecode', 'lastLoginTime']);
            $this->password = $this->getDI()->get('security')->hash($_POST['password']);
        }else{
            $this->skipAttributesOnUpdate(['account', 'password', 'lastStatecode', 'lastLoginTime']);
        }

        // 處理地址欄位
        $this->setAddress();
    }


    public function beforeValidation()
    {
        /**
         * use iterator loop for filter special columns
         * */
        foreach ($this as $key => $value) {
            if( in_array($key, ["ID", "marriage", "occupation", "annualIncome", "addressCity", "addressDistrict", "addressRoad", "addressLane", "addressAlley", "addressNo", "addressNoEx", "addressFloor", "addressFloorEx","lineID", "email"]) ){
                $this->{$key} = $this->getDI()->get('filter')->sanitize($value, "zerotonull");
            }
        }
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add('email',
            new \Phalcon\Validation\Validator\Email(array(
                'model'     => $this,
                'field'     => 'email',
                'allowEmpty'=> true,
                'message'   => '您輸入的電子郵箱格式錯誤，請重新輸入。'
        ) ) );

        $validator->add( 'account',
            new \Phalcon\Validation\Validator\Uniqueness([
                'model' => $this,
                'allowEmpty'=> true,
                'message' => '抱歉！目前設定帳號已經被使用，請重新處理。',
            ])
        );

        return $this->validate($validator);
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleInformation[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleInformation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people_information';
    }

}
