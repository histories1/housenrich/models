<?php

namespace Houserich\Models;

class PeopleInvoice extends \Phalcon\Mvc\Model
{

    /**
     * @comment('關聯會員主鍵')
     * @var integer
     */
    public $PeopleId;

    /**
     * @comment('買受人/抬頭')
     * @var string
     */
    public $title;

    /**
     * @comment('統一編號')
     * @var string
     */
    public $NO;

    /**
     * @comment('聯絡電話')
     * @var string
     */
    public $phone;

    /**
     * @comment('收件地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));
    }

    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane.'巷';
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley.'弄';
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNo.'號';
        }
        if( !empty($this->addressNoEx) ){
            $addr[] = '之'.$this->addressNoEx;
        }
        if( !empty($this->addressFloor) ){
            $addr[] = $this->addressFloor.'樓';
        }
        if( !empty($this->addressFloorEx) ){
            $addr[] = '之'.$this->addressFloorEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidationOncreate()
    {
    }


    public function beforeValidation()
    {
        // 處理地址欄位
        $this->setAddress();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people_invoice';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleInvoice[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleInvoice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
