<?php

namespace Houserich\Models;

class PeopleReserve extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $ppId;

    /**
     * @Comment("關聯會員編號")
     *
     * @var integer
     */
    public $PeopleId;

    /**
     * @Comment("關聯物件編號")
     *
     * @var integer
     */
    public $CrawdataId;

    /**
     * @Comment("物件標題")
     *
     * @var integer
     */
    public $title;

    /**
     * @Comment("設定預約時間")
     *
     * @var integer
     */
    public $setTime;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));
        $this->belongsTo('CrawldataId', 'Houserich\Models\Crawldata', 'crawldataId', array('alias' => 'Crawldata'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people_reserve';
    }


    public function beforeValidationOncreate()
    {
        // 設定預約時間
        $this->setTime = time();
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleReserve[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleReserve
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
