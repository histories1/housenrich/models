<?php

namespace Houserich\Models;

class PeopleRole extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $proleId;

    /**
     * @comment('關聯使用者編號')
     * @var integer
     */
    public $PeopleId;

    /**
     * @comment('關聯角色編號')
     * @var integer
     */
    public $RoleId;

    /**
     * @comment('狀態碼')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('設定記錄時間')
     * @var string
     */
    public $setTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RoleId', 'Houserich\Models\Role', 'roleId', array('alias' => 'Role'));
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people_role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleRole[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleRole
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
