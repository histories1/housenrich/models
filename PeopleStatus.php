<?php

namespace Houserich\Models;

class PeopleStatus extends \Phalcon\Mvc\Model
{
    /**
     * @comment('主鍵')
     * @var integer
     */
    public $pstatusId;

    /**
     * @comment('使用者編號')
     * @var integer
     */
    public $PeopleId;

    /**
     * @comment('狀態碼：正常、停權、禁止部分操作、待審核')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('狀態記錄時間')
     * @var integer
     */
    public $setTime;

    /**
     * @comment('設定者編號')
     * @var integer
     */
    public $SetterId;

    /**
     * @comment('備註')
     * @var string
     */
    public $comments;

    const STAT_INIT = 0;

    const STAT_AUTHED = 1;

    const STAT_SUSPEND = 2;

    const STAT_REMOVED = 3;

    var $statecodeLabels = ["未驗證會員", "已通過驗證", "停止權限", "已移除"];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));
    }

    public function getStatecodeLabel($statecode=null){
        if( isset($statecode) ){
            return $this->statecodeLabels[$statecode];
        }else{
            return $this->statecodeLabels[$this->statecode];
        }
    }


    public function beforeValidationOnCreate()
    {
        $this->setTime = time();
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'people_status';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleStatus[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PeopleStatus
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
