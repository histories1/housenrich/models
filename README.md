# Houserich好多房資料架構

主要大項為*物件(case)*、*使用者(user)*、*歸納法則(collection)*等。

## 房屋物件模型
> 以case為開頭相關資訊，case、case_append、case_status等基礎新增物件必寫入table。

- (case)[Case.php] == 物件主紀錄表
> 除新增必要欄位外，以另外存放列表資訊顯示與搜尋欄位
- (case_appends)[Case.php] == 物件延伸欄位資訊
> 價格[房屋/附屬/單價] / 管理費[總/車位/單坪價] / 稅務[房屋稅/地價稅/土地增值稅/契稅]
- (case_buildinginfo)[Case.php] == 物件附屬建物資訊
>
- (case_buildingregisteration)[Case.php] == 物件所屬建物謄本資訊
> 所有權人多組[姓名\統編] / 瀏覽數 / 追蹤總數
- (case_extendinfo)[Case.php] == 物件附屬欄位資訊
>
- (case_landregisteration)[Case.php] == 物件所屬土地謄本資訊
>
- (case_media)[Case.php] == 物件所屬多媒體資訊
> 包含(預設圖、照片集、格局圖、環景圖、其他附件、屋況說明圖等檔案)
- (case_mrt)[Case.php] == 物件所屬周遭捷運站資訊
>
- (case_parking)[Case.php] == 物件所屬車位資訊
>
- (case_school) == 物件所屬學區資訊
>
- (case_status) == 物件狀態紀錄資訊
>
- (case_viewhouse) == 物件看屋條件資訊
>
