<?php

namespace Houserich\Models;

class Richitem extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $richitemId;

    /**
     * @comment('案件編號')
     * @var string
     */
    public $richitemNo;

    /**
     * @comment('標題')
     * @var string
     */
    public $title;

    /**
     * @comment('用途')
     * @var string
     */
    public $usefor;

    /**
     * @comment('型態')
     * @var string
     */
    public $type;

    /**
     * @comment('完整地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * @comment('委託人姓名')
     * @var string
     */
    public $clientName;

    /**
     * @comment('委託人電話')
     * @var string
     */
    public $clientPhone;

    /**
     * @comment('委託人身分證字號')
     * @var string
     */
    public $clientID;

    /**
     * @comment('委託人生日')
     * @var string
     */
    public $clientBirthday;

    /**
     * @comment('關聯社區大樓編號')
     * @var integer
     */
    public $CommunityId;

    /**
     * @comment('社區名稱')
     * @var string
     */
    public $CommunityName;

    /**
     * @comment('建設公司')
     * @var string
     */
    public $company;

    /**
     * @comment('公共設施')
     * @var string
     */
    public $publicFacility;

    /**
     * @comment('建材')
     * @var string
     */
    public $meterials;

    /**
     * @comment('完成日期')
     * @var string
     */
    public $completion;

    /**
     * @comment('屋齡(年)')
     * @var integer
     */
    public $houseAgeYear;

    /**
     * @comment('屋齡(月)')
     * @var integer
     */
    public $houseAgeMonth;

    /**
     * @comment('總層數（地上）')
     * @var integer
     */
    public $floorAbove;

    /**
     * @comment('總層數（地下）')
     * @var integer
     */
    public $floorBelow;

    /**
     * @comment('路寬(m)')
     * @var integer
     */
    public $roadWidth;

    /**
     * @comment('電梯數量')
     * @var integer
     */
    public $numElevators;

    /**
     * @comment('1樓現況')
     * @var string
     */
    public $onefloor;

    /**
     * @comment('法定用途')
     * @var string
     */
    public $useforStatutory;

    /**
     * @comment('出售層數')
     * @var string
     */
    public $saleFloor;

    /**
     * @comment('該層戶數')
     * @var integer
     */
    public $saleHouseholdNum;

    /**
     * @comment('該層高度(m)')
     * @var integer
     */
    public $saleFloorHigh;

    /**
     * @comment('物件介紹(內文)')
     * @var string
     */
    public $description;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('richitemId', 'Houserich\Models\RichitemCommunitymrt', 'RichitemId', array('alias' => 'RichitemCommunitymrt'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemOnwer', 'RichitemId', array('alias' => 'RichitemOnwer'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemMedia', 'RichitemId', array('alias' => 'RichitemMedia'));
        $this->hasOne('richitemId', 'Houserich\Models\RichitemInformation', 'RichitemId', array('alias' => 'INFOS'));

        $this->hasMany('richitemId', 'Houserich\Models\RichitemAreaLand', 'RichitemId', array('alias' => 'RAL'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemAreaMainbuilding', 'RichitemId', array('alias' => 'RAMb'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemAreaAppendbuilding', 'RichitemId', array('alias' => 'RAAb'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemAreaSharepart', 'RichitemId', array('alias' => 'RAS'));
        $this->hasMany('richitemId', 'Houserich\Models\RichitemAreaParking', 'RichitemId', array('alias' => 'RAP'));
    }


    /**
     * 處理完整地址切割配置
     * */
    public function splitAddress()
    {
        $fields = ["addressCity", "addressDistrict", "addressRoad", "addressLane", "addressAlley", "addressNoEx", "addressFloor", "addressFloorEx", ];

        $c=0;
        foreach( $fields as $field ){
            if( empty($this->${$field}) ){
                $c++;
            }
        }

        // 超過3個欄位沒有內容
        if( $c>3 && !empty($this->address) ){
            // 市
            preg_match("/^(.+市).+/u", $this->address, $c);
            // var_dump($c);
            if( count($c) > 0 ){ $this->addressCity = $c[1]; }
            // 區
            preg_match("/^.+市(.+區)/u", $this->address, $d);
            // var_dump($d);
            if( count($d) > 0 ){ $this->addressDistrict = $d[1]; }
            // 路/街/道
            preg_match("/區(.+[街|路|道](.+段)?).+/u", $this->address, $r);
            // var_dump($r);
            if( count($r) > 0 ){ $this->addressRoad = $r[1]; }
            // 弄
            preg_match("/([\d+])弄/u", $this->address, $a);
            // var_dump($a);
            if( count($a) > 0 ){ $this->addressLane = $a[1]; }
            // 巷
            preg_match("/([\d+])巷/u", $this->address, $l);
            // var_dump($l);
            if( count($l) > 0 ){ $this->addressAlley = $l[1]; }
            // 號
            preg_match("/([\d+])號/u", $this->address, $n);
            // var_dump($n);
            if( count($n) > 0 ){ $this->addressNo = $n[1]; }
            // 號之
            preg_match("/號之([\d+])/u", $this->address, $nx);
            // var_dump($nx);
            if( count($nx) > 0 ){ $this->addressNoEx = $nx[1]; }
            // 樓
            preg_match("/([\d+])樓/u", $this->address, $f);
            // var_dump($f);
            if( count($f) > 0 ){ $this->addressFloor = $f[1]; }
            // 樓之
            preg_match("/樓之([\d+])/u", $this->address, $fx);
            // var_dump($fx);
            if( count($fx) > 0 ){ $this->addressFloorEx = $fx[1]; }
        }

    }


    /**
     * 處理完整地址組合配置
     * */
    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane.'巷';
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley.'弄';
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNo.'號';
        }
        if( !empty($this->addressNoEx) ){
            $addr[] = '之'.$this->addressNoEx;
        }
        if( !empty($this->addressFloor) ){
            $addr[] = $this->addressFloor.'樓';
        }
        if( !empty($this->addressFloorEx) ){
            $addr[] = '之'.$this->addressFloorEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidation()
    {
        $this->setAddress();

        // html
        $description = $this->description;
        $this->description = htmlspecialchars($description, ENT_QUOTES, 'UTF-8');

        // 判斷寫入fieldoptions
        // 用途
        if( $this->usefor == '其他' ){
            $fieldname='用途';
            $value = $_POST['useforothers'];
            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
                $f->fieldname = $fieldname;
                $f->label = $value;
                $f->value = $value;
                $f->save();
            }
            $this->usefor = $value;
        }

        // 法定用途
        if( $this->useforStatutory == '其他' ){
            $fieldname='法定用途';
            $value = $_POST['useforStatutoryothers'];
            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
                $f->fieldname = $fieldname;
                $f->label = $value;
                $f->value = $value;
                $f->save();
            }
            $this->useforStatutory = $value;
        }

        // 公共設施
        if( !empty($this->publicFacility) ){
        foreach($this->publicFacility as $i => $item) {
            if( $item == '其他' ){
                $fieldname='公共設施';
                $value = $_POST['publicFacilityothers'];
                $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
                if( !$f ){
                    $f = new \Houserich\Models\Fieldoptions();
                    $f->fieldname = $fieldname;
                    $f->label = $value;
                    $f->value = $value;
                    $f->save();
                }
                $this->publicFacility[$i] = $value;
            }
        }
        $this->publicFacility = implode(',', $this->publicFacility);
        }

        // 建材
        if( $this->meterials == '其他' ){
            $fieldname='建材';
            $value = $_POST['meterialsothers'];
            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
                $f->fieldname = $fieldname;
                $f->label = $value;
                $f->value = $value;
                $f->save();
            }
            $this->meterials = $value;
        }


        /**
         * use iterator loop for filter special columns
         * */
        foreach ($this as $key => $value) {
            if( in_array($key, ["addressLane", "addressAlley", "addressNo", "addressNoEx", "addressFloor", "addressFloorEx", "CommunityId", "floorAbove", "floorBelow", "roadWidth", "numElevators", "saleFloor", "saleHouseholdNum", "saleFloorHigh"]) ){
                $this->{$key} = $this->getDI()->get('filter')->sanitize($value, "zerotonull");
            }
        }
    }


    /**
     * 處理計算建物面積公式
     * 1.
     * */
    public function afterFetch()
    {
        // 土地面積
        $land = $this->RAL;

        // 主建物
        $mainbuilding = $this->RAMb;
        $mP = 0;
        if( count($mainbuilding) > 0 ){
        foreach ($mainbuilding as $mb) {
            $mP+=$mb->area;
        }
        }

        $appendbuilding = $this->RAAb;
        $aP = 0;
        if( count($appendbuilding) > 0 ){
        foreach ($appendbuilding as $ab) {
            $aP+=$ab->area;
        }
        }

        $share = $this->RAS;
        $sP = 0;
        if( count($share) > 0 ){
        foreach ($share as $sa) {
            $sP +=$sa->area;
        }
        }

        $parking = $this->RAP;
        $pP = 0;
        if( count($parking) > 0 ){
        foreach ($parking as $pa) {
            $pP +=$pa->area;
        }
        }

        $this->totalArea = $mP+$aP+$sP+$pP;
        $this->parkingArea = $pP;

        // 格局處理
        $hPn = '';
        $htxt = array();
        if( $this->INFOS->houseRoom ){
            $htxt[]=$this->INFOS->houseRoom.'房';
        }
        if( $this->INFOS->houseHall ){
            $htxt[]=$this->INFOS->houseHall.'廳';
        }
        if( $this->INFOS->houseToilt ){
            $htxt[]=$this->INFOS->houseHall.'衛';
        }
        if( $this->INFOS->houseRoomS ){
            $htxt[]=$this->INFOS->houseRoomS.'室';
        }
        if( $this->INFOS->houseBalcony ){
            $htxt[]=$this->INFOS->houseBalcony.'陽台';
        }
        $hPn = implode("/", $htxt);

        $aPn = '';
        $atxt = array();
        if( $this->INFOS->houseRoom ){
            $atxt[]=$this->INFOS->houseRoom.'房';
        }
        if( $this->INFOS->houseHall ){
            $atxt[]=$this->INFOS->houseHall.'廳';
        }
        if( $this->INFOS->houseToilt ){
            $atxt[]=$this->INFOS->houseHall.'衛';
        }
        if( $this->INFOS->houseRoomS ){
            $atxt[]=$this->INFOS->houseRoomS.'室';
        }
        if( $this->INFOS->houseBalcony ){
            $atxt[]=$this->INFOS->houseBalcony.'陽台';
        }

        if( count($atxt) > 0 ){
            $aPn = "（加蓋：".implode("/", $atxt)."）";
        }

        $this->patternString = $hPn.$aPn;

        // 含車位價 priceParking
        $this->priceTotal = $this->INFOS->priceRichitem+$this->INFOS->priceParking;

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Richitem[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Richitem
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem';
    }

}
