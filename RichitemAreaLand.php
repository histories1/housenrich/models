<?php

namespace Houserich\Models;

class RichitemAreaLand extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $rlaId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('使用分區')
     * @var string
     */
    public $landVaation;

    /**
     * @comment('現況')
     * @var string
     */
    public $situation;

    /**
     * @comment('面積(平方公尺)')
     * @var double
     */
    public $areaM;

    /**
     * @comment('面積(坪)')
     * @var double
     */
    public $area;

    /**
     * @comment('權力範圍(分子)')
     * @var double
     */
    public $scopeAuthorityMolecular;

    /**
     * @comment('權力範圍(分母)')
     * @var double
     */
    public $scopeAuthorityDenominator;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }


    public function beforeValidation()
    {
        // 判斷寫入fieldoptions
        // 使用分區
        if( $this->landVaation == '其他' ){
            $fieldname='使用分區';
            $value = $_POST['landVaationothers'];
            $f = \Houserich\Models\Fieldoptions::findFirst(["fieldname=:f: AND label=:l:", "bind"=>['f'=>$fieldname, 'l'=>$value]]);
            if( !$f ){
                $f = new \Houserich\Models\Fieldoptions();
                $f->fieldname = $fieldname;
                $f->label = $value;
                $f->value = $value;
                $f->save();
            }
            $this->landVaation = $value;
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_area_land';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaLand[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaLand
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
