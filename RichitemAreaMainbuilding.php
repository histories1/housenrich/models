<?php

namespace Houserich\Models;

class RichitemAreaMainbuilding extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $rmbaId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('層次')
     * @var string
     */
    public $layer;

    /**
     * @comment('面積(平方公尺)')
     * @var double
     */
    public $areaM;

    /**
     * @comment('面積(坪)')
     * @var double
     */
    public $area;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_area_mainbuilding';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaMainbuilding[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaMainbuilding
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
