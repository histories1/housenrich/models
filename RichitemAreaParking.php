<?php

namespace Houserich\Models;

class RichitemAreaParking extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $riparkingId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('車位樓層')
     * @var string
     */
    public $floor;

    /**
     * @comment('車位位置')
     * @var string
     */
    public $position;

    /**
     * @comment('產權')
     * @var string
     */
    public $property;

    /**
     * @comment('產權無')
     * @var string
     */
    public $propertyN;

    /**
     * @comment('產權有(法律)')
     * @var string
     */
    public $propertyYLaw;

    /**
     * @comment('產權有(權狀)')
     * @var string
     */
    public $propertyYRight;

    /**
     * @comment('種類')
     * @var string
     */
    public $type;

    /**
     * @comment('長(cm)')
     * @var integer
     */
    public $long;

    /**
     * @comment('寬(cm)')
     * @var integer
     */
    public $width;

    /**
     * @comment('高(cm)')
     * @var integer
     */
    public $height;

    /**
     * @comment('載重(kg)')
     * @var integer
     */
    public $weight;

    /**
     * @comment('面積(平方公尺)')
     * @var double
     */
    public $areaM;

    /**
     * @comment('面積(坪)')
     * @var double
     */
    public $area;

    /**
     * @comment('權力範圍(分子)')
     * @var double
     */
    public $scopeAuthorityMolecular;

    /**
     * @comment('權力範圍(分母)')
     * @var double
     */
    public $scopeAuthorityDenominator;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_area_parking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaParking[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemAreaParking
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
