<?php

namespace Houserich\Models;

class RichitemCommunitymrt extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $rcmId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('關聯社區大樓編號')
     * @var integer
     */
    public $CommunityId;

    /**
     * @comment('關聯捷運站資訊編號')
     * @var integer
     */
    public $MrtId;

    /**
     * @comment('計算距離(m)')
     * @var double
     */
    public $distance;

    /**
     * @comment('捷運站狀態')
     * @var integer
     */
    public $mrtStatus;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('CommunityId', 'Houserich\Models\Community', 'communityId', array('alias' => 'Community'));
        $this->belongsTo('MrtId', 'Houserich\Models\Mrt', 'mrtId', array('alias' => 'Mrt'));
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_communitymrt';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemCommunitymrt[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemCommunitymrt
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
