<?php

namespace Houserich\Models;

class RichitemInformation extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $riId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('土地總面積(坪)')
     * @var double
     */
    public $areaLand;

    /**
     * @comment('主建物總面積(坪)')
     * @var double
     */
    public $areaMainbuilding;

    /**
     * @comment('附屬建物總面積(坪)')
     * @var double
     */
    public $areaAppendbuilding;

    /**
     * @comment('共有部分總面積(坪)')
     * @var double
     */
    public $areaSharepart;

    /**
     * @comment('車位總面積(坪)')
     * @var double
     */
    public $areaParking;

    /**
     * @comment('權狀總面積(坪)')
     * @var double
     */
    public $areaRight;

    /**
     * @comment('使用總面積(坪)')
     * @var double
     */
    public $areaUsing;

    /**
     * @comment('公設比(%)')
     * @var double
     */
    public $ratioPublicArea;

    /**
     * @comment('總價(萬元)')
     * @var double
     */
    public $priceRichitem;

    /**
     * @comment('車位總價(萬元)')
     * @var double
     */
    public $priceParking;

    /**
     * @comment('單價(萬元/坪)')
     * @var double
     */
    public $priceSingle;

    /**
     * @comment('房屋格局(衛)')
     * @var integer
     */
    public $houseToilt;

    /**
     * @comment('房屋格局(廳)')
     * @var integer
     */
    public $houseHall;

    /**
     * @comment('房屋格局(房)')
     * @var integer
     */
    public $houseRoom;

    /**
     * @comment('房屋格局(室)')
     * @var integer
     */
    public $houseRoomS;

    /**
     * @comment('房屋格局(陽台)')
     * @var integer
     */
    public $houseBalcony;

    /**
     * @comment('加蓋格局(房)')
     * @var integer
     */
    public $addonRoom;

    /**
     * @comment('加蓋格局(廳)')
     * @var integer
     */
    public $addonHall;

    /**
     * @comment('加蓋格局(衛)')
     * @var integer
     */
    public $addonToilt;

    /**
     * @comment('加蓋格局(室)')
     * @var integer
     */
    public $addonRoomS;

    /**
     * @comment('加蓋格局(陽台)')
     * @var integer
     */
    public $addonBalcony;

    /**
     * @comment('地下室格局(房)')
     * @var integer
     */
    public $basementRoom;

    /**
     * @comment('地下室格局(廳)')
     * @var integer
     */
    public $basementHall;

    /**
     * @comment('地下室格局(衛)')
     * @var integer
     */
    public $basementToilt;

    /**
     * @comment('地下室格局(室)')
     * @var integer
     */
    public $basementRoomS;

    /**
     * @comment('地下室格局(陽台)')
     * @var integer
     */
    public $basementBalcony;

    /**
     * @comment('採光')
     * @var string
     */
    public $lighting;

    /**
     * @comment('廚房瓦斯')
     * @var string
     */
    public $gasKitchen;

    /**
     * @comment('浴室瓦斯')
     * @var string
     */
    public $gasBathroom;

    /**
     * @comment('管理費(元)')
     * @var double
     */
    public $manageFee;

    /**
     * @comment('其他')
     * @var string
     */
    public $others;

    /**
     * @comment('房屋現況')
     * @var string
     */
    public $houseSituation;

    /**
     * @comment('房屋租金(元)')
     * @var double
     */
    public $houseRent;

    /**
     * @comment('房屋租約到期日')
     * @var string
     */
    public $houseRentEnddate;

    /**
     * @comment('加蓋現況')
     * @var string
     */
    public $addonSituation;

    /**
     * @comment('加蓋租金(元)')
     * @var double
     */
    public $addonRent;

    /**
     * @comment('加蓋租約到期日')
     * @var string
     */
    public $addonRentEnddate;

    /**
     * @comment('車位現況')
     * @var string
     */
    public $parkingSituation;

    /**
     * @comment('車位租金(元)')
     * @var double
     */
    public $parkingRent;

    /**
     * @comment('車位租約到期日')
     * @var string
     */
    public $parkingRentEnddate;

    /**
     * @comment('房屋投資報酬率(%)')
     * @var double
     */
    public $houseReturnInvest;

    /**
     * @comment('含加蓋投資報酬率(%)')
     * @var double
     */
    public $withaddonReturnInvest;

    /**
     * @comment('大樓朝向')
     * @var string
     */
    public $facetoBuilding;

    /**
     * @comment('大門朝向')
     * @var string
     */
    public $facetoDoor;

    /**
     * @comment('客廳窗朝向')
     * @var string
     */
    public $facetoWindow;

    /**
     * @comment('房屋現值(元)')
     * @var double
     */
    public $houseValue;

    /**
     * @comment('房屋稅(元)')
     * @var double
     */
    public $taxHouse;

    /**
     * @comment('申報地價(元)')
     * @var double
     */
    public $landValue;

    /**
     * @comment('地價稅(元)')
     * @var double
     */
    public $taxLand;

    /**
     * @comment('土地增值稅(元)')
     * @var double
     */
    public $taxLandAdd;

    /**
     * @comment('契稅')
     * @var double
     */
    public $taxContract;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }


    public function beforeValidation()
    {
        /**
         * use iterator loop for filter special columns
         * */
        foreach ($this as $key => $value) {
            if( in_array($key, ["houseToilt", "houseHall", "houseRoom", "houseRoomS", "houseBalcony", "addonRoom", "addonHall", "addonToilt", "addonRoomS", "addonBalcony", "basementRoom", "basementHall", "basementToilt", "basementRoomS", "basementBalcony", "manageFee", "houseRent", "houseRentEnddate", "addonRent", "addonRentEnddate", "parkingRent", "parkingRentEnddate", "houseReturnInvest", "withaddonReturnInvest", "houseValue", "taxHouse", "landValue", "taxLand", "taxLandAdd", "taxContract"]) ){
                $this->{$key} = $this->getDI()->get('filter')->sanitize($value, "zerotonull");
            }
        }
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_information';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemInformation[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemInformation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
