<?php

namespace Houserich\Models;

class RichitemMedia extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $mediaId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('媒體檔類型')
     * @var integer
     */
    public $type;

    /**
     * @comment('檔案格式')
     * @var string
     */
    public $mime;

    /**
     * @comment('檔案儲存路徑')
     * @var string
     */
    public $filepath;

    /**
     * @comment('存取識別碼')
     * @var string
     */
    public $UUID;

    /**
     * @comment('原始檔名')
     * @var string
     */
    public $originname;

    /**
     * @comment('檔案大小')
     * @var integer
     */
    public $size;

    /**
     * @comment('縮圖路徑')
     * @var string
     */
    public $thumbnail;

    /**
     * @comment('檔案產生時間')
     * @var integer
     */
    public $setTime;

    /**
     * @comment('狀態碼')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('排序')
     * @var integer
     */
    public $weight;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', '\Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }


    public function beforeValidation()
    {
        $this->setTime = time();
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_media';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemMedia[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemMedia
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
