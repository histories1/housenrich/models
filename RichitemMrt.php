<?php

namespace Houserich\Models;

class RichitemMrt extends \Phalcon\Mvc\Model
{

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('關聯捷運站資訊編號')
     * @var integer
     */
    public $MrtId;

    /**
     * @comment('計算距離(m)')
     * @var double
     */
    public $distance;

    /**
     * @comment('捷運站狀態')
     * @var string
     */
    public $mrtStatus;

    /**
     * @comment('類型:車站/出口')
     * @var string
     */
    public $type;

    /**
     * @comment('關聯物件座標點編號')
     * @var integer
     */
    public $RichitemGeomarkerId;

    /**
     * @comment('關聯捷運站座標點編號')
     * @var integer
     */
    public $MrtGeomarkerId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('MrtId', 'Houserich\Models\Mrt', 'mrtId', array('alias' => 'Mrt'));
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
        $this->belongsTo('MrtGeomarkerId', 'Houserich\Models\Geomarkers', 'geomarkersId', array('alias' => 'Geomarkers'));
        $this->belongsTo('RichitemGeomarkerId', 'Houserich\Models\Geomarkers', 'geomarkersId', array('alias' => 'Geomarkers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_mrt';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemMrt[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemMrt
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
