<?php

namespace Houserich\Models;

class RichitemOwner extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $roId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('所有權人')
     * @var string
     */
    public $name;

    /**
     * @comment('所有權人統編/身分證字號')
     * @var string
     */
    public $ID;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_owner';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemOnwer[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemOnwer
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
