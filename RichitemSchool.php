<?php

namespace Houserich\Models;

class RichitemSchool extends \Phalcon\Mvc\Model
{
    /**
     * @comment('主鍵')
     * @var integer
     */
    public $rsId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('關聯學區編號')
     * @var integer
     */
    public $SchoolId;

    /**
     * @comment('級別(國中/國小)')
     * @var string
     */
    public $level;

    /**
     * @comment('是否為預設學區')
     * @var integer
     */
    public $default;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
        $this->belongsTo('SchoolId', 'Houserich\Models\School', 'schoolId', array('alias' => 'School'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_school';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemSchool[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemSchool
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
