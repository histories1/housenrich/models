<?php

namespace Houserich\Models;

class RichitemStatus extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $ristatusId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('狀態碼')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('狀態記錄時間')
     * @var integer
     */
    public $setTime;

    /**
     * @comment('關聯使用者編號')
     * @var integer
     */
    public $SetterId;

    /**
     * @comment('備註')
     * @var string
     */
    public $comments;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_status';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemStatus[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemStatus
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
