<?php

namespace Houserich\Models;

class RichitemViewhouse extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $riviewhouseId;

    /**
     * @comment('關聯物件編號')
     * @var integer
     */
    public $RichitemId;

    /**
     * @comment('可看屋日')
     * @var string
     */
    public $bookweekday;

    /**
     * @comment('可看屋時間')
     * @var string
     */
    public $booktime;

    /**
     * @comment('看屋見面地點')
     * @var string
     */
    public $meetplace;

    /**
     * @comment('鑰匙存放位置')
     * @var string
     */
    public $keyat;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('RichitemId', 'Houserich\Models\Richitem', 'richitemId', array('alias' => 'Richitem'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemViewhouse[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RichitemViewhouse
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'richitem_viewhouse';
    }

}
