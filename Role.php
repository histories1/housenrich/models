<?php

namespace Houserich\Models;

class Role extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $roleId;

    /**
     * @comment('角色')
     * @var string
     */
    public $role;

    /**
     * @comment('顯示名稱')
     * @var string
     */
    public $label;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('roleId', 'Houserich\Models\PeopleRole', 'RoleId', array('alias' => 'PeopleRole'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'role';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Role[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Role
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
