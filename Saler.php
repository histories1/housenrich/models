<?php

namespace Houserich\Models;

class Saler extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $salerId;

    /**
     * @comment('關聯會員編號')
     * @var integer
     */
    public $PeopleId;

    /**
     * @comment('姓名')
     * @var string
     */
    public $fullName;

    /**
     * @comment('性別')
     * @var string
     */
    public $gender;

    /**
     * @comment('手機門號')
     * @var string
     */
    public $cellphone;

    /**
     * @comment('物件地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * @comment('房屋(坪)')
     * @var double
     */
    public $areaHouse;

    /**
     * @comment('車位(坪)')
     * @var double
     */
    public $arreaParking;

    /**
     * @comment('物件類型')
     * @var string
     */
    public $type;

    /**
     * @comment('價格(萬)')
     * @var double
     */
    public $price;

    /**
     * @comment('社區名稱')
     * @var string
     */
    public $community;

    /**
     * @comment('紀錄產生時間')
     * @var string
     */
    public $setTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('PeopleId', 'Houserich\Models\People', 'peopleId', array('alias' => 'People'));
    }


    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane.'巷';
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley.'弄';
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNo.'號';
        }
        if( !empty($this->addressNoEx) ){
            $addr[] = '之'.$this->addressNoEx;
        }
        if( !empty($this->addressFloor) ){
            $addr[] = $this->addressFloor.'樓';
        }
        if( !empty($this->addressFloorEx) ){
            $addr[] = '之'.$this->addressFloorEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidation()
    {
        $this->setAddress();
    }

    public function beforeValidationOncreate()
    {
        // 時間
        $this->setTime = time();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'saler';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Saler[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Saler
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
