<?php

namespace Houserich\Models;

class School extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $schoolId;

    /**
     * @comment('學校')
     * @var string
     */
    public $name;

    /**
     * @comment('地址')
     * @var string
     */
    public $address;

    /**
     * @comment('地址(縣市)')
     * @var string
     */
    public $addressCity;

    /**
     * @comment('地址(行政區)')
     * @var string
     */
    public $addressDistrict;

    /**
     * @comment('地址(路)')
     * @var string
     */
    public $addressRoad;

    /**
     * @comment('地址(巷)')
     * @var integer
     */
    public $addressLane;

    /**
     * @comment('地址(弄)')
     * @var integer
     */
    public $addressAlley;

    /**
     * @comment('地址(號)')
     * @var integer
     */
    public $addressNo;

    /**
     * @comment('地址(號之)')
     * @var integer
     */
    public $addressNoEx;

    /**
     * @comment('地址(樓)')
     * @var integer
     */
    public $addressFloor;

    /**
     * @comment('地址(樓之)')
     * @var integer
     */
    public $addressFloorEx;

    /**
     * @comment('經度')
     * @var double
     */
    public $longitude;

    /**
     * @comment('緯度')
     * @var double
     */
    public $latitude;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('schoolId', 'Houserich\Models\RichitemSchool', 'SchoolId', array('alias' => 'RichitemSchool'));
        $this->hasMany('schoolId', 'Houserich\Models\RichitemSchool', 'SchoolId', NULL);
    }


    /**
     * 處理完整地址切割配置
     * */
    public function splitAddress()
    {
        $fields = ["addressCity", "addressDistrict", "addressRoad", "addressLane", "addressAlley", "addressNoEx", "addressFloor", "addressFloorEx", ];

        $c=0;
        foreach( $fields as $field ){
            if( empty($this->${$field}) ){
                $c++;
            }
        }

        // 超過3個欄位沒有內容
        if( $c>3 && !empty($this->address) ){
            // 市
            preg_match("/^(.+市).+/u", $this->address, $c);
            // var_dump($c);
            if( count($c) > 0 ){ $this->addressCity = $c[1]; }
            // 區
            preg_match("/^.+市(.+區)/u", $this->address, $d);
            // var_dump($d);
            if( count($d) > 0 ){ $this->addressDistrict = $d[1]; }
            // 路/街/道
            preg_match("/區(.+[街|路|道](.+段)?).+/u", $this->address, $r);
            // var_dump($r);
            if( count($r) > 0 ){ $this->addressRoad = $r[1]; }
            // 弄
            preg_match("/([\d+])弄/u", $this->address, $a);
            // var_dump($a);
            if( count($a) > 0 ){ $this->addressLane = $a[1]; }
            // 巷
            preg_match("/([\d+])巷/u", $this->address, $l);
            // var_dump($l);
            if( count($l) > 0 ){ $this->addressAlley = $l[1]; }
            // 號
            preg_match("/([\d+])號/u", $this->address, $n);
            // var_dump($n);
            if( count($n) > 0 ){ $this->addressNo = $n[1]; }
            // 號之
            preg_match("/號之([\d+])/u", $this->address, $nx);
            // var_dump($nx);
            if( count($nx) > 0 ){ $this->addressNoEx = $nx[1]; }
            // 樓
            preg_match("/([\d+])樓/u", $this->address, $f);
            // var_dump($f);
            if( count($f) > 0 ){ $this->addressFloor = $f[1]; }
            // 樓之
            preg_match("/樓之([\d+])/u", $this->address, $fx);
            // var_dump($fx);
            if( count($fx) > 0 ){ $this->addressFloorEx = $fx[1]; }
        }

    }


    /**
     * 處理完整地址組合配置
     * */
    public function setAddress() {
        $addr[] = $this->addressCity;
        $addr[] = $this->addressDistrict;
        $addr[] = $this->addressRoad;

        if( !empty($this->addressLane) ){
            $addr[] = $this->addressLane.'巷';
        }
        if( !empty($this->addressAlley) ){
            $addr[] = $this->addressAlley.'弄';
        }
        if( !empty($this->addressNo) ){
            $addr[] = $this->addressNo.'號';
        }
        if( !empty($this->addressNoEx) ){
            $addr[] = '之'.$this->addressNoEx;
        }
        if( !empty($this->addressFloor) ){
            $addr[] = $this->addressFloor.'樓';
        }
        if( !empty($this->addressFloorEx) ){
            $addr[] = '之'.$this->addressFloorEx;
        }

        $this->address = implode('', $addr);
    }


    public function beforeValidation()
    {
        $this->setAddress();
    }


    public function afterFetch()
    {
        $this->splitAddress();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'school';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return School[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return School
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
