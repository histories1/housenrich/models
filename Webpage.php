<?php

namespace Houserich\Models;

class Webpage extends \Phalcon\Mvc\Model
{

    /**
     * @comment('主鍵')
     * @var integer
     */
    public $webpageId;

    /**
     * @comment('類型')
     * @var integer
     */
    public $type;

    /**
     * @comment('內容')
     * @var string
     */
    public $content;

    /**
     * @comment('標題')
     * @var string
     */
    public $title;

    /**
     * @comment('建立者')
     * @var integer
     */
    public $UserId;

    /**
     * @comment('狀態')
     * @var integer
     */
    public $statecode;

    /**
     * @comment('建立時間')
     * @var string
     */
    public $initTime;

    /**
     * @comment('來源')
     * @var string
     */
    public $source;

    /**
     * @comment('外部連結')
     * @var string
     */
    public $sourcelink;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'webpage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Webpage[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Webpage
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
